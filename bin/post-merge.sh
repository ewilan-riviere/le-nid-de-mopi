#!/bin/bash

# supervisorctl stop p1pdd-worker
# supervisorctl stop p1pdd-production-worker

composer install
composer dump-autoload
php8.2 artisan log:clear
php8.2 artisan optimize:fresh
# php8.2 artisan scout:fresh
pnpm i
pnpm build

# supervisorctl start p1pdd-worker
# supervisorctl start p1pdd-production-worker

notifier discord 'Nid de Mopi deployed'
