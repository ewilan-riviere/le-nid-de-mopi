/// <reference types="vite/client" />

declare global {
  interface Window {
    appUrl: string
  }
}

window.appUrl = window.appUrl || ''

export {}
