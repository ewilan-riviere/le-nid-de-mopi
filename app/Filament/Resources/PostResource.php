<?php

namespace App\Filament\Resources;

use App\Filament\Resources\PostResource\Pages;
use App\Filament\Resources\PostResource\RelationManagers\CategoriesRelationManager;
use App\Models\Post;
use Filament\Forms\Components;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Kiwilan\Steward\Enums\PublishStatusEnum;
use Kiwilan\Steward\Filament\Components\AutoPicture;
use Kiwilan\Steward\Filament\Components\MetaBlock;
use Kiwilan\Steward\Filament\Components\NameInput;
use Kiwilan\Steward\Filament\Components\SeoBlock;
use Kiwilan\Steward\Filament\Config\FilamentLayout;

class PostResource extends Resource
{
    protected static ?string $model = Post::class;

    protected static ?string $navigationIcon = 'heroicon-o-newspaper';

    public static function form(Form $form): Form
    {
        return FilamentLayout::make($form)
            ->schema([
                FilamentLayout::column([
                    FilamentLayout::section([
                        NameInput::make('title')
                            ->label('Title')
                            ->autofocus()
                            ->required(),
                        Components\TextInput::make('time_to_read_minutes')
                            ->label('Time to read')
                            ->disabled()
                            ->suffix(' min')
                            ->hint('Auto-calculated'),
                        Components\Select::make('categories')
                            ->label('Categories')
                            ->multiple()
                            ->relationship('categories', 'name')
                            ->preload(),
                        Components\Select::make('author_id')
                            ->label('Author')
                            ->relationship('author', 'name')
                            ->preload()
                            ->required()
                            ->default(auth()->id()),
                    ]),
                    FilamentLayout::section([
                        Components\RichEditor::make('body')
                            ->required()
                            ->placeholder('Enter post body')
                            ->columnSpan(2),
                    ]),
                ]),
                FilamentLayout::column([
                    FilamentLayout::section([
                        // AutoPicture::make('picture')
                        //     ->label('Picture')
                        //     ->imageEditor(),
                        PublishStatusEnum::toggleButtons(),
                        Components\DatePicker::make('published_at')
                            ->label('Published at / Scheduled for')
                            ->default(now()),
                        Components\FileUpload::make('audio')
                            ->label('Audio')
                            ->directory('audio'),
                    ]),
                    FilamentLayout::section([
                        SeoBlock::make(),
                    ]),
                    FilamentLayout::section([
                        MetaBlock::make(),
                    ]),
                ], width: 1),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                // Tables\Columns\ImageColumn::make('picture')
                //     ->circular(),
                Tables\Columns\TextColumn::make('title')
                    ->searchable()
                    ->sortable(),
                Tables\Columns\TextColumn::make('time_to_read_minutes')
                    ->label('Time to read')
                    ->suffix(' min'),
                Tables\Columns\TextColumn::make('status')
                    ->badge()
                    ->sortable(),
                Tables\Columns\TextColumn::make('categories.name')
                    ->badge()
                    ->searchable(),
                Tables\Columns\TextColumn::make('author.name')
                    ->searchable()
                    ->sortable(),
                Tables\Columns\TextColumn::make('published_at')
                    ->dateTime('d/m/Y'),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime('d/m/Y')
                    ->toggleable()
                    ->toggledHiddenByDefault(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ])
            ->defaultSort('published_at', 'desc');
    }

    public static function getRelations(): array
    {
        return [
            CategoriesRelationManager::class,
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()->with(['categories']);
    }

    public static function getNavigationBadge(): ?string
    {
        return static::getModel()::where('status', PublishStatusEnum::draft)
            ->count();
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListPosts::route('/'),
            'create' => Pages\CreatePost::route('/create'),
            'edit' => Pages\EditPost::route('/{record}/edit'),
        ];
    }
}
