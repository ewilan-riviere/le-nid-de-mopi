<?php

namespace App\Utils;

use Illuminate\Database\Eloquent\Builder;

class Searching
{
    protected function __construct(
        protected string $value,
        protected array $results = [],
        protected int|false $limit = 20,
    ) {
    }

    /**
     * Search for movies, tv shows, collections, and members.
     */
    public static function search(string $value, int|false $limit = 20): self
    {
        $self = new self($value, [], $limit);

        $results = collect([]);

        $posts = $self->searchModel(\App\Models\Post::class);
        $results = $results->merge($posts);

        $results = $results->values();
        $self->results = $results->toArray();

        return $self;
    }

    public function results(): array
    {
        return $this->results;
    }

    private function searchModel(string $model)
    {
        /** @var Builder */
        $results = $model::search($this->value);

        if ($this->limit) {
            $results = $results->take($this->limit);
        }

        $results = $results->get();

        $results->map(function ($item) use ($model) {
            $type = str_replace('App\\Models\\', '', $model);
            $item['type'] = match ($type) {
                'Post' => 'Post',
                default => 'Unknown',
            };

            return $item;
        });

        return $results;
    }
}
