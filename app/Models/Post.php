<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kiwilan\Steward\Traits\HasSearchableName;
use Kiwilan\Steward\Traits\HasSeo;
use Kiwilan\Steward\Traits\HasSlug;
use Kiwilan\Steward\Traits\HasTimeToRead;
use Kiwilan\Steward\Traits\Publishable;
use Kiwilan\Steward\Traits\Queryable;
use Laravel\Scout\Searchable;

class Post extends Model
{
    use HasFactory;
    use HasSearchableName, Searchable {
        HasSearchableName::searchableAs insteadof Searchable;
    }
    use HasSeo;
    use HasSlug;
    use HasTimeToRead;
    use Publishable;
    use Queryable;

    protected $slugWith = 'title';

    protected $metaTitleFrom = 'title';

    protected $metaDescriptionFrom = 'body';

    protected $fillable = [
        'title',
        'body',
        'audio',
        'picture',
        'author_id',
    ];

    protected $appends = [
        'audio_file',
        'picture_file',
        'preview',
    ];

    protected $queryDefaultSort = 'title';

    protected $queryAllowedSorts = ['title', 'created_at', 'updated_at'];

    protected $queryPagination = 10;

    public function getAudioFileAttribute(): ?string
    {
        return $this->audio ? asset("storage/audio/{$this->audio}") : null;
    }

    public function getPictureFileAttribute(): ?string
    {
        return $this->picture ? asset("storage/pictures/{$this->picture}") : null;
    }

    public function getPreviewAttribute(): string
    {
        return substr(strip_tags($this->body), 0, 250).'...';
    }

    public function categories(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }

    public function author(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function toSearchableArray(): array
    {
        $this->loadMissing(['categories']);

        return [
            'id' => $this->id,
            'title' => $this->title,
            'body' => $this->body,
            'picture' => $this->picture,
            'categories' => $this->categories->pluck('name')->toArray(),
        ];
    }
}
