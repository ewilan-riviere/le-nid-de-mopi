<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kiwilan\Steward\Traits\HasSeo;
use Kiwilan\Steward\Traits\HasSlug;

class Page extends Model
{
    use HasFactory;
    use HasSeo;
    use HasSlug;

    protected $slugWith = 'title';

    protected $metaTitleFrom = 'title';

    protected $metaDescriptionFrom = 'body';

    protected $fillable = [
        'title',
        'body',
    ];
}
