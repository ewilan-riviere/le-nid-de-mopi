<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use Kiwilan\Steward\Queries\HttpQuery;
use Spatie\RouteAttributes\Attributes\Get;
use Spatie\RouteAttributes\Attributes\Prefix;

#[Prefix('articles')]
class PostController extends Controller
{
    #[Get('/', name: 'articles.index')]
    public function index(Request $request)
    {
        return inertia('Home', [
            'query' => HttpQuery::for(Post::published(), $request)->inertia(),
        ]);
    }

    #[Get('/{post_slug}', name: 'articles.show')]
    public function show(Post $post)
    {
        return inertia('Home', [
            'post' => $post,
        ]);
    }
}
