<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use Kiwilan\Steward\Queries\HttpQuery;
use Spatie\RouteAttributes\Attributes\Get;

class HomeController extends Controller
{
    #[Get('/', name: 'home')]
    public function index(Request $request)
    {
        return inertia('Home', [
            'query' => HttpQuery::for(Post::published(), $request)->inertia(),
        ]);
    }
}
