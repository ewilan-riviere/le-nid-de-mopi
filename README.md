# **Le Nid de Mopi**

[![php][php-version-src]][php-version-href]
[![laravel][laravel-version-src]][laravel-version-href]
[![node][node-version-src]][node-version-href]
[![tests][tests-src]][tests-href]

## Installation

Download dependencies

```bash
composer i
pnpm i
```

Copy `.env.example` to `.env` and fill it.

```bash
cp .env.example .env
```

Generate key

```bash
php artisan key:generate
```

Storage link

```bash
php artisan storage:link
```

Run migrations

```bash
php artisan migrate:fresh --seed
```

Serve application

```bash
php artisan serve
```

You can access to application on <http://localhost:8000>, keep serve command and compile assets

```bash
pnpm dev
```

## Requests

> Titre : pouvoir faire une recherche dessus
> Date de publication
>
> Chapô : début d'article qui s'affiche en preview
>
> Contenu : reste de l'article
>
> Catégorie : pouvoir en mettre plusieurs, pourvoir faire une recherche sur les catégorie ou un filtre
> Page qui liste les articles par ordre chronologique :
>
> Petit cadre pour chaque preview d'article avec le titre, la date de publication, le chapô et les catégories.
> De même pour les pages qui liste les articles par catégorie.
> (S'il te faut autre chose, ou davantage de précision, faut pas hésiter à demander)
> Je ne sais pas si on a besoin de la date.
> Genre c'est pratique pour trier et voir les nouvelles choses...

-   [x] pages statiques avec éditeur simple
-   [ ] crawler pour les articles
-   [x] ajouter des fichiers audio

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[laravel-version-src]: https://img.shields.io/badge/v10-laravel?label=Laravel&color=777bb4&logo=laravel&logoColor=ffffff&labelColor=18181b
[laravel-version-href]: https://laravel.com/
[php-version-src]: https://img.shields.io/badge/v8.2-php?label=PHP&color=777bb4&logoColor=ffffff&labelColor=18181b
[php-version-href]: https://www.php.net/
[node-version-src]: https://img.shields.io/badge/v18-nodejs?label=Node.js&color=777bb4&logoColor=ffffff&labelColor=18181b
[node-version-href]: https://nodejs.org/en
[tests-src]: https://gitlab.com/ewilan-riviere/le-nid-de-mopi/badges/main/pipeline.svg
[tests-href]: https://gitlab.com/ewilan-riviere/le-nid-de-mopi/-/pipelines
