/* eslint-disable eslint-comments/no-unlimited-disable */
/* eslint-disable */
/* prettier-ignore */
// @ts-nocheck
// Generated by unplugin-svg-transformer
export type SvgName = 'arrow-down' | 'arrow-right' | 'chevron-down' | 'x-mark' | 'default'
export const options = {
  fallback: "<svg xmlns=\"http://www.w3.org/2000/svg\" fill=\"none\" viewBox=\"0 0 24 24\" stroke=\"currentColor\" stroke-width=\"2\"><path stroke-linecap=\"round\" stroke-linejoin=\"round\" d=\"M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z\" /></svg>",
  svg: {
    clearSize: "none",
    clearClass: "none",
    clearStyle: "none",
    currentColor: false,
    sizeInherit: true
  },
  warning: true,
  cacheDir: "./node_modules/unplugin-svg-transformer/cache",
  global: true,
  libraryDir: "./resources/js",
  svgDir: "./resources/svg",
  useTypes: true
}
export const svgList: Record<SvgName, () => Promise<{ default: string }>> = {
  'arrow-down': () => import('../../node_modules/unplugin-svg-transformer/cache/arrow-down.ts'),
  'arrow-right': () => import('../../node_modules/unplugin-svg-transformer/cache/arrow-right.ts'),
  'chevron-down': () => import('../../node_modules/unplugin-svg-transformer/cache/chevron-down.ts'),
  'x-mark': () => import('../../node_modules/unplugin-svg-transformer/cache/x-mark.ts'),
  'default': () => import('../../node_modules/unplugin-svg-transformer/cache/default.ts'),
}

export async function importSvg(name: SvgName): Promise<string> {
  if (!svgList[name] && options.warning)
    console.warn(`Icon ${name} not found`)
  const icon = svgList[name] || svgList["default"]
  const svg = await icon()

  return svg.default
}

if (typeof window !== 'undefined') {
  window.ust = window.ust || {}
  window.ust.options = options
  window.ust.svgList = svgList
  window.ust.importSvg = importSvg
}
