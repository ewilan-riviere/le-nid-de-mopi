<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Kiwilan\Steward\Utils\Faker;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Category>
 */
class CategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $faker = Faker::make();

        return [
            'name' => $faker->text()->tag(),
        ];
    }
}
