<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Kiwilan\Steward\Enums\PublishStatusEnum;
use Kiwilan\Steward\Utils\Faker;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $faker = Faker::make();

        $audio_path = null;
        $add_audio = $this->faker->boolean(25);
        if ($add_audio) {
            $name = uniqid();
            $base_path = storage_path('app/public/audio');
            if (! file_exists($base_path)) {
                mkdir($base_path, 0775, true);
            }
            copy(database_path('seeders/media/audio.mp3'), $base_path."/{$name}.mp3");
            $audio_path = "audio/{$name}.mp3";
        }

        $add_picture = $this->faker->boolean(25);
        $picture_path = null;
        if ($add_picture) {
            $name = uniqid();
            $base_path = storage_path('app/public/pictures');
            if (! file_exists($base_path)) {
                mkdir($base_path, 0775, true);
            }
            copy(public_path('images/background.webp'), $base_path."/{$name}.jpg");
            $picture_path = "pictures/{$name}.jpg";
        }

        return [
            'title' => $faker->text()->title(),
            'body' => $faker->richText()
                ->noImage()
                ->paragraphs(),
            'audio' => $add_audio ? $audio_path : null,
            'picture' => $add_picture ? $picture_path : null,
            'published_at' => $faker->dateTime()->timestamps()->getCreatedAt(),
            'status' => $this->faker->randomElement(PublishStatusEnum::toDatabase()),
        ];
    }

    public function author(): self
    {
        $users = User::all();

        return $this->state(fn (array $attributes) => [
            'author_id' => $users->random()->id,
        ]);
    }
}
