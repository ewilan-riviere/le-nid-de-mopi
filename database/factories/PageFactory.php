<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Kiwilan\Steward\Utils\Faker;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Page>
 */
class PageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $faker = Faker::make();

        return [
            'title' => $faker->text()->title(),
            'body' => $faker->richText()
                ->noImage()
                ->paragraphs(),
        ];
    }
}
