<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Kiwilan\Steward\Services\DirectoryService;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        $directory = DirectoryService::make();
        $directory->clearDirectory(storage_path('app/public'));

        $this->call([
            EmptySeeder::class,
            CategorySeeder::class,
            PostSeeder::class,
            PageSeeder::class,
        ]);
    }
}
