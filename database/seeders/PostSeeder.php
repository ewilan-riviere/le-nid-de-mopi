<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $posts = Post::factory()
            ->author()
            ->count(50)
            ->create();
        $categories = Category::all();

        $posts->each(function (Post $post) use ($categories) {
            $categories = $categories->random(rand(1, 3));
            $post->categories()->saveMany($categories);
        });
    }
}
